package com.lipipo.lipipobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LipipoBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(LipipoBackendApplication.class, args);
	}

}

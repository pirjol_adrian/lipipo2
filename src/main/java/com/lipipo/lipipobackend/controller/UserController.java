package com.lipipo.lipipobackend.controller;

import com.lipipo.lipipobackend.model.User;
import com.lipipo.lipipobackend.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UserService userService;



    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user) {
        user.setDateTime(LocalDateTime.now());
        User savedUser = userService.createUser(user);
        return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
    }
    @GetMapping("{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") Long userId){
        User user = userService.getUserById(userId);
        return ResponseEntity.ok(user);
    }
    @GetMapping
    public ResponseEntity<List<User>> getAllUser(){
        return new ResponseEntity<>(userService.getAllUser(), HttpStatus.CREATED);
    }
    @PutMapping("{id}")
    public ResponseEntity<User> updateUser(@PathVariable("id") Long userId,@RequestBody User updateUser){
        return ResponseEntity.ok(userService.updateUser(userId, updateUser));
    }
    @DeleteMapping("{id}")
    public ResponseEntity<String> deleyeUser(@PathVariable("id") Long userId){
        userService.deleteUser(userId);
        return  ResponseEntity.ok("User was deleted successfully!.");
    }
}

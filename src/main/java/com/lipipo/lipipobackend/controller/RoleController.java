package com.lipipo.lipipobackend.controller;

import com.lipipo.lipipobackend.model.Role;
import com.lipipo.lipipobackend.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/role")
public class RoleController {
    private final RoleService roleService;
    @PostMapping
    public ResponseEntity<Role> createRole(@RequestBody Role role){
        return new ResponseEntity<>(roleService.createRole(role), HttpStatus.CREATED);
    }
    @GetMapping
    public ResponseEntity<List<Role>> getAllRole(){
        return new ResponseEntity<>(roleService.getAllRole(), HttpStatus.CREATED);
    }
    @PutMapping("{id}")
    public ResponseEntity<Role> updateRole(@PathVariable("id") Long roleId,@RequestBody Role updateRole){
        return ResponseEntity.ok(roleService.updateRole(roleId, updateRole));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deleyeRole(@PathVariable("id") Long roleId){
        roleService.deleteRole(roleId);
        return  ResponseEntity.ok("Role was deleted successfully!.");
    }
    @GetMapping("{id}")
    public ResponseEntity<Role> getRoleById(@PathVariable("id") Long roleId){
        Role role = roleService.getRoleById(roleId);
        return ResponseEntity.ok(role);
    }
}

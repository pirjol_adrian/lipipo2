package com.lipipo.lipipobackend.service;

import com.lipipo.lipipobackend.model.Role;
import com.lipipo.lipipobackend.repository.RoleRepository;
import com.lipipo.lipipobackend.service.iService.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService implements IRoleService{
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role createRole(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Role getRoleById(Long roleId) {
        return roleRepository.findById(roleId).orElseThrow(() -> new IllegalStateException(String.format("Role with %s id dosen't exist", roleId)));
    }

    @Override
    public List<Role> getAllRole() {
        return roleRepository.findAll();
    }

    @Override
    public Role updateRole(Long roleId, Role role) {
        Role roleToUpdated = roleRepository.findById(roleId).orElseThrow(() -> new IllegalStateException(String.format(
                "User with %s id dosen't exist", roleId)));
        roleToUpdated.setRole(role.getRole());
        return roleRepository.save(roleToUpdated);
    }

    @Override
    public void deleteRole(Long roleId) {
        Role roleToDelete = roleRepository.findById(roleId)
                .orElseThrow(() -> new IllegalStateException(String.format("User with %s id  dosen't exist", roleId)));
        roleRepository.deleteById(roleId);

    }

}

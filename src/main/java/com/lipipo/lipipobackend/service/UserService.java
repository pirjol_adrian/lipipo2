package com.lipipo.lipipobackend.service;

import com.lipipo.lipipobackend.model.Address;
import com.lipipo.lipipobackend.model.Role;
import com.lipipo.lipipobackend.model.User;
import com.lipipo.lipipobackend.repository.UserRepository;
import com.lipipo.lipipobackend.service.iService.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class UserService implements IUserService {

    @Autowired
    private AddressService addressService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleService roleService;

    @Override
    public User createUser(User user) {
        List<Address> addresses = user.getAddresses();
        List<Role> roles = user.getRoles();

        if (addresses != null) {
            for (Address address : addresses) {
                if (address.getId() == null) {
                    addressService.createAddress(address);
                }
            }
        }

        return userRepository.save(user);
    }

    @Override
    public User getUserById(Long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new IllegalStateException(String.format("User with %s id dosen't exist", userId)));
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public User updateUser(Long userId, User user) {
        User userToUpdated = userRepository.findById(userId).orElseThrow(() -> new IllegalStateException(String.format("User with %s id dosen't exist", userId)));

        if (user.getName() == null) {
            userToUpdated.setName(userToUpdated.getName());
        } else {
            userToUpdated.setName(user.getName());
        }

        if (user.getEmail() == null) {
            userToUpdated.setEmail(userToUpdated.getEmail());
        } else {
            userToUpdated.setEmail(user.getEmail());
        }

        if (user.getPassword() == null) {
            userToUpdated.setPassword(userToUpdated.getPassword());
        } else {
            userToUpdated.setPassword(user.getPassword());
        }

        if (user.getCnp() == null) {
            userToUpdated.setCnp(userToUpdated.getCnp());
        } else {
            userToUpdated.setCnp(user.getCnp());
        }

        List<Address> addresses = userToUpdated.getAddresses();
        List<Address> addresses2 = user.getAddresses();
        Address addressToSave = new Address();

        if (addresses2 != null) {
            for (Address address2 : addresses2) {
                addressToSave = address2;
            }
            if (addresses != null) {
                for (Address address : addresses) {
                    if (address.getId() == null) {
                        addressService.createAddress(address);
                    } else {
                        addressService.updateAddress(address.getId(), addressToSave);
                    }
                }
            }
        }

        //HINT urmareste sa aloci rolurile existente si extrase din baza de date din hibernate
        //De terminat sa updatez rolul
        List<Role> dataBaseRole = roleService.getAllRole();
        List<Role> frontendRole = user.getRoles();
        List<Role> tempSetRole = new ArrayList<>();

        for (Role role : dataBaseRole) {
            for (Role role1 : frontendRole) {
                if (role1.getRole().equalsIgnoreCase(role.getRole())) {
                    tempSetRole.add(role);
                }
            }
        }
        userToUpdated.setRoles(tempSetRole);
        return userRepository.save(userToUpdated);
    }

    @Override
    public void deleteUser(Long userId) {
        User userToDelete = userRepository.findById(userId)
                .orElseThrow(() -> new IllegalStateException(String.format("User with %s id  dosen't exist", userId)));

        for (Address address : userToDelete.getAddresses()) {
            address.getUsers().remove(userToDelete);
        }

        userRepository.deleteById(userId);
    }
}

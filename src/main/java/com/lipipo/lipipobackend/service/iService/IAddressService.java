package com.lipipo.lipipobackend.service.iService;

import com.lipipo.lipipobackend.model.Address;
import com.lipipo.lipipobackend.model.User;

import java.util.List;

public interface IAddressService {
    public Address createAddress(Address address);
    public Address getAddressById(Long addressId);
    public List<Address> getAllAddress();
    public Address updateAddress(Long addressId, Address address);
    void deleteAddress(Long addressId);
}

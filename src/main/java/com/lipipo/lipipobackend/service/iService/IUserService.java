package com.lipipo.lipipobackend.service.iService;

import com.lipipo.lipipobackend.model.User;

import java.util.List;

public interface IUserService {
    public User createUser(User user);
    public User getUserById(Long userId);
    public List<User> getAllUser();
    public User updateUser(Long userId, User user);
    void deleteUser(Long userId);
}

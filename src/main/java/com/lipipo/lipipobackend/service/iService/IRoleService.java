package com.lipipo.lipipobackend.service.iService;

import com.lipipo.lipipobackend.model.Address;
import com.lipipo.lipipobackend.model.Role;

import java.util.List;

public interface IRoleService {
    public Role createRole(Role role);
    public Role getRoleById(Long roleId);
    public List<Role> getAllRole();
    public Role updateRole(Long roleId, Role role);
    void deleteRole(Long roleId);
}

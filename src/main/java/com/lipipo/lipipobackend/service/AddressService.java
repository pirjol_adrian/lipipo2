package com.lipipo.lipipobackend.service;

import com.lipipo.lipipobackend.model.Address;
import com.lipipo.lipipobackend.repository.AddressRepository;
import com.lipipo.lipipobackend.service.iService.IAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService implements IAddressService {
    @Autowired
    private AddressRepository addressRepository;
        public Address createAddress(Address address) {
        return addressRepository.save(address);
    }

    public Address updateAddress(Long id, Address address){
            Address addressToUpdate = addressRepository.findById(id)
                    .orElseThrow(()-> new IllegalStateException(String.format("Address with %s id  dosen't exist", id)));
            if(address.getRegion()!=null){
                addressToUpdate.setRegion(address.getRegion());
            }else{
                addressToUpdate.setRegion(addressToUpdate.getRegion());
            }

            if(address.getCity()!=null){
                addressToUpdate.setCity(address.getCity());
            }else{
                addressToUpdate.setCity(addressToUpdate.getCity());
            }

            if(address.getAddress()!=null){
                addressToUpdate.setAddress(address.getAddress());
            }else{
                addressToUpdate.setAddress(addressToUpdate.getAddress());
            }
           return addressRepository.save(addressToUpdate);
        }

    public void deleteAddress(Long addressId) {
        Address addressToDelete = addressRepository.findById(addressId)
                .orElseThrow(()-> new IllegalStateException(String.format("Address with %s id  dosen't exist", addressId)));

        addressRepository.deleteById(addressId);
    }

    @Override
    public Address getAddressById(Long addressId) {
        return null;
    }

    @Override
    public List<Address> getAllAddress() {
        return null;
    }
}
